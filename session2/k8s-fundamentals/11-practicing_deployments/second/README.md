# Guide:
 
Requisites: make sure that the docker and k8s are connected `docker container ls` if there is none the run `eval $(minikube -p minikube docker-env)`

 1. Docker image:
```
docker build -t app2 .
docker image ls | grep app2
```
2. Test the image is working:
```
docker run -ti -p 8080:80 --rm app2
```

open browser on http://localhost:8080 you should see the page with `v2.0.0` text


---

## K8s part:
1. Namespace.
```
kubectl apply -f namespace.yaml
```

2. Deployment or Pod.
```
kubectl apply -f deployment.yaml 
```

3. Service that poitns tho the pod.
```
kubectl apply -f service.yaml 
```

4. Ingress to access the service.
```
kubectl apply -f ingress.yaml 
```
