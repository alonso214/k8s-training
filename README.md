# Sesson 1
- Installing Docker: https://academy.learnk8s.io/containers-2
- Gettings started with Docker: https://academy.learnk8s.io/containers-3
- Creating images: https://academy.learnk8s.io/containers-4
- Managing state: https://academy.learnk8s.io/containers-5
- Networking: https://academy.learnk8s.io/containers-6
- Packaging an application: https://academy.learnk8s.io/containers-7
- Debugging: https://academy.learnk8s.io/containers-8
- Labs: https://academy.learnk8s.io/container-challenges-intro

# Session 2
- Prerequisites: https://academy.learnk8s.io/fundamentals-prerequisites
- WARNING for Windows: use minikube start --driver=hyperv  (or --driver=virtualbox if you already set that up on your windows)
- WARNING for Ubuntu/Linux: use minikube start --driver=virtualbox
- Creating the app: https://academy.learnk8s.io/fundamentals-create-app
- Setting up the cluster: https://academy.learnk8s.io/fundamentals-cluster
- Your first deployment: https://academy.learnk8s.io/fundamentals-first-deployment
- Self-healing and scaling: https://academy.learnk8s.io/fundamentals-scaling
- Organising resources: https://academy.learnk8s.io/fundamentals-namespaces
- Labs: https://academy.learnk8s.io/fundamentals-labs
- Challenges: https://academy.learnk8s.io/fundamentals-challenges

# Session 3
- Before you start: https://academy.learnk8s.io/advanced-deployment-strategies-prereq
- Rolling updates: https://academy.learnk8s.io/advanced-deployment-strategies-rolling
- Services and selectors: https://academy.learnk8s.io/advanced-deployment-strategies-selector
- Canary deployments: https://academy.learnk8s.io/advanced-deployment-strategies-canary
- Blue-green deployments: https://academy.learnk8s.io/advanced-deployment-strategies-bluegreen
- Rolling back a deployment: https://academy.learnk8s.io/advanced-deployment-strategies-rollbacks
- Challenges: https://academy.learnk8s.io/advanced-deployment-strategies-challenges
- Enter your solutions here: https://academy.learnk8s.io/courses/advanced-deployment-strategies

# Session 4
- Prerequisites: https://academy.learnk8s.io/architecture-install
- Bootstrapping a cluster: https://academy.learnk8s.io/architecture-bootstrap
- Deploy an application: https://academy.learnk8s.io/architecture-deploy
- Explore the API server: https://academy.learnk8s.io/architecture-api
- The kubelet: https://academy.learnk8s.io/architecture-kubelet
- Testing resiliency: https://academy.learnk8s.io/architecture-resiliency

# Session 5
- Bootstrapping a cluster: https://academy.learnk8s.io/networking-bootstrap
- Deploying a two-tier application: https://academy.learnk8s.io/networking-deploy
- Exploring the Endpoints: https://academy.learnk8s.io/networking-endpoints
- kube-proxy: https://academy.learnk8s.io/networking-kubeproxy
- Exposing the application: https://academy.learnk8s.io/networking-expose
- Using an Ingress: https://academy.learnk8s.io/networking-ingress
- Service discovery: https://academy.learnk8s.io/networking-service-discovery
- Lab: https://academy.learnk8s.io/networking-labs

# Session 6
- Deploying a stateless MySQL: https://academy.learnk8s.io/managing-state-deploy
- Persisting changes: https://academy.learnk8s.io/managing-state-persistence
- Dynamic provisioning: https://academy.learnk8s.io/managing-state-dynamic-provisioning
- Lab: https://academy.learnk8s.io/managing-state-lab
